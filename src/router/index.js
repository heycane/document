import Vue from 'vue'
import Router from 'vue-router'
import FrontEnd from '@/pages/frontEnd/FrontEnd'
import DetailList from '@/pages/detailList/DetailList'
import DetailInfo from '@/pages/detailInfo/DetailInfo'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '',
    redirect: '/home'
  }, {
    path: '/:path',
    name: 'FrontEnd',
    component: FrontEnd
  }, {
    path: '/:path/:name',
    name: 'DetailList',
    component: DetailList
  }, {
    path: '/:path/:name/:item',
    name: 'DetailInfo',
    component: DetailInfo
  }]
})
