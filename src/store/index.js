import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

function findPath (arr, path) {
  return arr.findIndex((item) => {
    return item.path === path
  })
}
function localStorageSetItem (arr) {
  return localStorage.setItem('homeList', JSON.stringify(arr))
}

export default new Vuex.Store({
  state: {
    homeList: JSON.parse(localStorage.getItem('homeList')) || []
  },
  mutations: {
    addHomeList (state, todo) {
      state.homeList.push(todo)
      localStorageSetItem(state.homeList)
    },
    removeHomeList (state, path) {
      let list = state.homeList
      let index = findPath(list, path)
      list.splice(index, 1)
      localStorageSetItem(state.homeList)
    }
  }
})
